#include "Gra.h"
#include "GraFactory.h"


HRESULT STDMETHODCALLTYPE Gra::QueryInterface(REFIID iid, void **ptr) {
	if(ptr == NULL) return E_POINTER;
	*ptr = NULL;
	if(iid == IID_IUnknown) *ptr = this;
	else if(iid == IID_IGra) *ptr = this;
	if(*ptr != NULL) { AddRef(); return S_OK; };
	return E_NOINTERFACE;
};

ULONG STDMETHODCALLTYPE Gra::AddRef() {
	InterlockedIncrement(&m_ref);
	return m_ref;
};

ULONG STDMETHODCALLTYPE Gra::Release() {
	ULONG rv = InterlockedDecrement(&m_ref);
	if(rv == 0) delete this;
	return rv;
};

volatile ULONG usageCount;
Gra::Gra(){
	InterlockedIncrement(&usageCount);
	m_ref = 0;
	qb = QuatroBoard();
}

Gra::~Gra(){
	InterlockedDecrement(&usageCount);
}



extern "C"  HRESULT  __stdcall DllCanUnloadNow() {
	return usageCount > 0 ? S_FALSE : S_OK;
};


extern "C"  HRESULT  __stdcall DllGetClassObject(REFCLSID cls, REFIID iid, void **ptr) {
	if(ptr == NULL) return E_INVALIDARG;
	*ptr = NULL;
	if(cls != CLSID_Gra) return CLASS_E_CLASSNOTAVAILABLE;
	if(iid != IID_IClassFactory) return CLASS_E_CLASSNOTAVAILABLE;
	GraFactory *fact = new (std::nothrow) GraFactory();
	if(fact == NULL) return E_OUTOFMEMORY;
	HRESULT rv = fact->QueryInterface(iid, ptr);
	if(FAILED(rv)) { delete fact; *ptr = NULL; };
	
	return rv;
};