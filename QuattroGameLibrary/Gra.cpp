#include "Gra.h"
#include "Helper.h"

// {E814DC57-2EF3-464B-AEFA-50783B535FEE}
static const GUID IID_IGra = 
{ 0xe814dc57, 0x2ef3, 0x464b, { 0xae, 0xfa, 0x50, 0x78, 0x3b, 0x53, 0x5f, 0xee } };

// {A56D7A40-4FBF-480A-A6F0-2D7EF2164565}
static const GUID CLSID_Gra =
{ 0xa56d7a40, 0x4fbf, 0x480a, { 0xa6, 0xf0, 0x2d, 0x7e, 0xf2, 0x16, 0x45, 0x65 } };

extern ITypeLib *lib;
extern ITypeInfo *info;

HRESULT STDMETHODCALLTYPE Gra::CzyWygrana(bool &res ,int board[BOARD_SIZE][BOARD_SIZE]){
	qb.setBoard(board);
	res = qb.havePalyerWon();
	return S_OK;




	HRESULT rc;
	rc = LoadRegTypeLib(LIBID_KSRGra, 1, 0, LANG_NEUTRAL, &lib);
	if(FAILED(rc)) { /* obs�uga b��du */ };
	rc = lib->GetTypeInfoOfGuid(IID_IGra, &info);
	if(lib) lib->Release();
}

HRESULT STDMETHODCALLTYPE Gra::CzyMozna(bool &res, char msg[MAX_MSG_LEN], int board[BOARD_SIZE][BOARD_SIZE], int x, int y, bool red, bool tall, bool solid, bool rect)
{
	if(board!=NULL)
		qb.setBoard(board);
	string msgStr;
	res = qb.move(msgStr,x,y,red,tall,solid,rect);
	coptyStringToArr(msgStr,msg);
	return S_OK;
}

HRESULT STDMETHODCALLTYPE Gra::Move(bool &res, char msg[MAX_MSG_LEN], int x, int y, bool red, bool tall, bool solid, bool rect){
	
	string msgStr;
	res = qb.move(msgStr,x,y,red,tall,solid,rect);
	coptyStringToArr(msgStr,msg);
	
	return S_OK;
}

HRESULT STDMETHODCALLTYPE Gra::GetPrompt(char msg[MAX_MSG_LEN]){
	string msgStr;
	msgStr = qb.prompt();
	coptyStringToArr(msgStr,msg);
	
	return S_OK;
}

HRESULT STDMETHODCALLTYPE Gra::GetBoard(char board[MAX_MSG_LEN]){
	string msgStr;
	msgStr = qb.getBoard();
	coptyStringToArr(msgStr,board);
	
	return S_OK;
}

HRESULT STDMETHODCALLTYPE Gra::IsWon(bool &isWon){
	isWon = qb.isWon();
	return S_OK;
}

HRESULT STDMETHODCALLTYPE Gra::IsWon(bool *isWon){
	*isWon = qb.isWon();
	return S_OK;
}