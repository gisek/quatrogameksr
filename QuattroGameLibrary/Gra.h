#pragma once

#include "QuattroBoard.h"
#include "IGra.h"
#include <sstream>
#include <stdio.h>

using namespace std;

class Gra : public IGra, IDispatch{
public:
	Gra();
	~Gra();
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void **ptr);
	virtual ULONG STDMETHODCALLTYPE AddRef();
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount(UINT *pctinfo);
	virtual HRESULT STDMETHODCALLTYPE GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo **ppTInfo);
	virtual HRESULT STDMETHODCALLTYPE GetIDsOfNames(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid, DISPID *rgDispId);
	virtual HRESULT STDMETHODCALLTYPE Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pDispParams, VARIANT *pVarResult, EXCEPINFO *pExcepInfo, UINT *puArgErr);


	virtual HRESULT STDMETHODCALLTYPE CzyWygrana(bool &res, int board[BOARD_SIZE][BOARD_SIZE]);
	virtual HRESULT STDMETHODCALLTYPE CzyMozna(bool &res, char msg[MAX_MSG_LEN], int board[BOARD_SIZE][BOARD_SIZE], int x, int y, bool red, bool tall, bool solid, bool rect);

	virtual HRESULT STDMETHODCALLTYPE Move(bool &res, char msg[MAX_MSG_LEN], int x, int y, bool red, bool tall, bool solid, bool rect);
	virtual HRESULT STDMETHODCALLTYPE GetPrompt(char msg[MAX_MSG_LEN]);
	virtual HRESULT STDMETHODCALLTYPE GetBoard(char board[MAX_MSG_LEN]);
	virtual HRESULT STDMETHODCALLTYPE IsWon(bool &isWon);
	virtual HRESULT STDMETHODCALLTYPE IsWon(bool *isWon);
private:
	volatile ULONG m_ref;
	
	QuatroBoard qb;

};
