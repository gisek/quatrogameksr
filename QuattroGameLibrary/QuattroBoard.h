#include <string.h>
#include <iostream>

using namespace std;

#define BOARD_SIZE 4

enum Pawn;

class QuatroBoard
{
private:
	int fields[BOARD_SIZE][BOARD_SIZE];
	int pawns[2][2][2][2];
	int player;
	bool won;
public:
	QuatroBoard();

	void setBoard(int fieldsOut[BOARD_SIZE][BOARD_SIZE]);
	bool move(string &msg, int x, int y, bool red, bool tall, bool solid, bool rect);
	bool havePalyerWon();
	string getBoard();
	bool isWon();
	string prompt();
};
