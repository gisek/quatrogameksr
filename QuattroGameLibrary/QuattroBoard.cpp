#include "QuattroBoard.h"
#include <sstream>

enum Pawn{
	Red = 1,
	Black =2,
	Tall= 4,
	Short=8,
	Solid= 16,
	Hollow=32,
	Rect= 64,
	Circ=128,
};


QuatroBoard::QuatroBoard(){
	for(int i = 0; i< BOARD_SIZE; i++)
		for(int j=0; j<BOARD_SIZE; j++)
			fields[i][j]=0;
	for(int i = 0; i< 2; i++)
		for(int j=0; j<2; j++)
			for(int k=0; k<2; k++)
				for(int l=0; l<2; l++)
					pawns[i][j][k][l]=true;
	player=1;
	won=false;
}

void QuatroBoard::setBoard(int fieldsOut[BOARD_SIZE][BOARD_SIZE]){
	for(int i = 0; i< BOARD_SIZE; i++)
		for(int j=0; j<BOARD_SIZE; j++)
			fields[i][j]=fieldsOut[i][j];
}

bool QuatroBoard::move(string &msg, int x, int y, bool red, bool tall, bool solid, bool rect){
	stringstream ss;

	if(won){
		ss << "Player: " << player << " already won" <<endl;
		msg=ss.str();
		return false;
	}

	if(pawns[red][tall][solid][rect] == false){
		ss << "Pawn already used" << endl;
		msg=ss.str();
		return false;
	}

	if(x <0 || x >= BOARD_SIZE || y <0 || y >= BOARD_SIZE){
		ss << "Selected field should fit the board" <<endl;
		msg=ss.str();
		return false;
	}

	if(fields[x][y] != 0){
		ss << "Field already taken" << endl;
		msg=ss.str();
		return false;
	}


	pawns[red][tall][solid][rect] = false;
	int pawn=0;
	pawn = pawn | (red ? Red : Black); 
	pawn = pawn | (tall ? Tall : Short); 
	pawn = pawn | (solid ? Solid : Hollow); 
	pawn = pawn | (rect ? Rect : Circ); 

	fields[x][y]=pawn;

	won = havePalyerWon();
	if(won){
		ss << "Player: " << player << " wins!" <<endl;
		msg=ss.str();
	}else{
		ss << "Player: " << player << " moved" <<endl;
		msg=ss.str();
		player = player==1 ? 2 : 1;
	}

	return true;
}

bool QuatroBoard::havePalyerWon(){

	//horizontal
	for(int i=0; i < BOARD_SIZE; i++){
		int res=0xFFFF;
		for(int j=0; j < BOARD_SIZE; j++){
			if(fields[i][j]==0)
			{
				res=0;
				break;
			}
			res = res & fields[i][j];
		}
		if(res > 0 && res!=0xFFFF)
			return true;
	}

	//vertical
	for(int i=0; i < BOARD_SIZE; i++){
		int res=0xFFFF;
		for(int j=0; j < BOARD_SIZE; j++){
			if(fields[j][i]==0)
			{
				res=0;
				break;
			}
			res = res & fields[j][i];
		}
		if(res > 0 && res!=0xFFFF)
			return true;
	}

	//diagonal 1
	int res= fields[0][0] & fields[1][1] & fields[2][2] & fields[3][3];
	if(res >0) 
		return true;

	//diagonal 2
	res = fields[0][3] & fields[1][2] & fields[2][1] & fields[3][0];
	if(res >0) 
		return true;

	return false;
}

string QuatroBoard::getBoard(){
	stringstream ss;

	for(int i = 0; i< BOARD_SIZE; i++){
		for(int j=0; j<BOARD_SIZE; j++){
			int f = fields[i][j];
			string field = "[----]";
			if(f != 0){
				char color = (f & Red) > 0 ? 'R' : 'B';
				char height = (f & Tall) > 0 ? 'T' : 'S';
				char impletion = (f & Solid) > 0 ? 'S' : 'H';
				char shape = (f & Rect) > 0 ? 'R' : 'C';

				ss<<'[' << color << height << impletion << shape << ']';
			}else{
				ss<<field;
			}
		}
		ss << '\n';
	}


	return ss.str();
}

bool QuatroBoard::isWon(){
	return won;
}

string QuatroBoard::prompt(){
	return "Select field (x y red tall solid rect): ";
}
