#pragma once

/*
Public interface for COM clients
*/

#include <Windows.h>


// {E814DC57-2EF3-464B-AEFA-50783B535FEE}
DEFINE_GUID(IID_IGra, 
0xe814dc57, 0x2ef3, 0x464b, 0xae, 0xfa, 0x50, 0x78, 0x3b, 0x53, 0x5f, 0xee);

// {A56D7A40-4FBF-480A-A6F0-2D7EF2164565}
DEFINE_GUID(CLSID_Gra , 
0xa56d7a40, 0x4fbf, 0x480a, 0xa6, 0xf0, 0x2d, 0x7e, 0xf2, 0x16, 0x45, 0x65);

// {CE03D4FA-229C-4081-B862-7B5378AB3E33}
static const GUID LIBID_KSRGra = 
{ 0xce03d4fa, 0x229c, 0x4081, { 0xb8, 0x62, 0x7b, 0x53, 0x78, 0xab, 0x3e, 0x33 } };


#define BOARD_SIZE 4
#define MAX_MSG_LEN 1000

class IGra : public IUnknown{
public:
	virtual HRESULT STDMETHODCALLTYPE CzyWygrana(bool &res, int board[BOARD_SIZE][BOARD_SIZE])=0;
	virtual HRESULT STDMETHODCALLTYPE CzyMozna(bool &res, char msg[MAX_MSG_LEN], int board[BOARD_SIZE][BOARD_SIZE], int x, int y, bool red, bool tall, bool solid, bool rect)=0;
	
	virtual HRESULT STDMETHODCALLTYPE Move(bool &res, char msg[MAX_MSG_LEN], int x, int y, bool red, bool tall, bool solid, bool rect)=0;
	virtual HRESULT STDMETHODCALLTYPE GetPrompt( char msg[MAX_MSG_LEN])=0;
	virtual HRESULT STDMETHODCALLTYPE GetBoard( char board[MAX_MSG_LEN])=0;
	virtual HRESULT STDMETHODCALLTYPE IsWon(bool &isWon)=0;
	virtual HRESULT STDMETHODCALLTYPE IsWon(bool *isWon)=0;
};