#include "Gra.h"

ITypeLib *lib = NULL;
ITypeInfo *info = NULL;

HRESULT STDMETHODCALLTYPE Gra::GetTypeInfoCount(UINT *count) {
	*count = 1;
	return S_OK;
};

HRESULT STDMETHODCALLTYPE Gra::GetTypeInfo(UINT typeInfo, LCID lcid,
	ITypeInfo **out) {
		*out = NULL;
		if(typeInfo != 0) return DISP_E_BADINDEX;
		info->AddRef();
		*out = info; // zak�adamy �e info jest polem Stosu lub globalne
		return S_OK;
};

HRESULT STDMETHODCALLTYPE Gra::GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames,
	UINT cNames, LCID lcid,
	DISPID *rgDispId) {
		if(riid != IID_NULL) return DISP_E_UNKNOWNINTERFACE;
		return DispGetIDsOfNames(info, rgszNames, cNames, rgDispId);
};

HRESULT STDMETHODCALLTYPE Gra::Invoke(DISPID dispIdMember, REFIID riid,
	LCID lcid, WORD wFlags,
	DISPPARAMS *pDispParams,
	VARIANT *pVarResult,
	EXCEPINFO *pExcepInfo,
	UINT *puArgErr) {
		if(riid != IID_NULL) return DISP_E_UNKNOWNINTERFACE;
		return DispInvoke(this, info, dispIdMember, wFlags, pDispParams,
			pVarResult, pExcepInfo, puArgErr); 
};