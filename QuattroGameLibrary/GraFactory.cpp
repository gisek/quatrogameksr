#include "GraFactory.h"
#include "Gra.h"

extern volatile ULONG usageCount;

GraFactory::GraFactory(){
	usageCount=0;
}

GraFactory::~GraFactory(){
}

HRESULT STDMETHODCALLTYPE GraFactory::QueryInterface(REFIID iid, void **ptr) {
	if(ptr == NULL) return E_POINTER;
	*ptr = NULL;
	if(iid == IID_IUnknown) *ptr = this;
	else if(iid == IID_IClassFactory) *ptr = this;
	if(*ptr != NULL) { AddRef(); return S_OK; };
	return E_NOINTERFACE;
};

ULONG STDMETHODCALLTYPE GraFactory::AddRef() {
	InterlockedIncrement(&m_ref);
	return m_ref;
};

ULONG STDMETHODCALLTYPE GraFactory::Release() {
	ULONG rv = InterlockedDecrement(&m_ref);
	if(rv == 0) delete this;
	return rv;
};

HRESULT STDMETHODCALLTYPE GraFactory::LockServer(BOOL lock) {
	if(lock) InterlockedIncrement(&usageCount);
	else InterlockedDecrement(&usageCount);
	return S_OK;
};

HRESULT STDMETHODCALLTYPE GraFactory::CreateInstance(IUnknown *outer,
	REFIID iid, void **ptr) {
	if(ptr == NULL) return E_POINTER;
	*ptr = NULL;
	if(iid != IID_IUnknown && iid != IID_IGra) return E_NOINTERFACE;
	// chcemy dosta� NULL zamiast wyj�tku std::badalloc
	Gra *obj = new (std::nothrow) Gra();
	if(obj == NULL) return E_OUTOFMEMORY;
	HRESULT rv = obj->QueryInterface(iid, ptr);
	if(FAILED(rv)) { delete obj; *ptr = NULL; };
	return rv;
};