#include<iostream>
#include<string>
#include <sstream>
#include<stdio.h>
#include<Windows.h>
#include "../QuattroGameLibrary/IGra.h"

using namespace std;

// {E814DC57-2EF3-464B-AEFA-50783B535FEE}
static const GUID IID_IGra = 
{ 0xe814dc57, 0x2ef3, 0x464b, { 0xae, 0xfa, 0x50, 0x78, 0x3b, 0x53, 0x5f, 0xee } };

// {A56D7A40-4FBF-480A-A6F0-2D7EF2164565}
static const GUID CLSID_Gra =
{ 0xa56d7a40, 0x4fbf, 0x480a, { 0xa6, 0xf0, 0x2d, 0x7e, 0xf2, 0x16, 0x45, 0x65 } };

void performMoves(IGra *g);

int main()
{
	CoInitialize(NULL);
	IGra *a;	

	HRESULT res = CoCreateInstance( CLSID_Gra, NULL, CLSCTX_INPROC_SERVER, IID_IGra, (void **)&a ); /* Utworzenie obiektu COM, nie robimy AddRef */
	{
		IUnknown *b = a;
		b->AddRef(); // zrobili�my kopi�, musimy zrobi� AddRef
		{
			performMoves(a);
		}
		b->Release(); // opuszczamy blok, b przestaje istnie�, robimy Release
	}
	return 0;
}

void clearMsg(char msg[MAX_MSG_LEN]){
	for(int i =0; i<MAX_MSG_LEN; i++)
		if(msg[i]==0)
			break;
		else
			msg[i]=0;
}

void performMoves(IGra *g){
	int moves[4][6]={
		{0,0,0,0,0,0},
		{0,1,0,0,0,1},
		{0,2,0,0,1,0},
		{0,3,0,0,1,1},
	};

	const int movesCount=sizeof(moves)/4/6;

	HRESULT res;

	bool result;
	char msg[MAX_MSG_LEN];

	res=g->GetBoard(msg);
	cout << msg << endl;

	for(int i = 0; i < movesCount; i++){
		
		res=g->GetPrompt(msg);
		cout << msg << endl;

		int x,y;
		bool red, tall, solid, rect;

		x=moves[i][0];
		y=moves[i][1];
		red=moves[i][2];
		tall=moves[i][3];
		solid=moves[i][4];
		rect=moves[i][5];
		
		bool moveRes;

		res=g->Move(moveRes, msg,x,y,red,tall,solid,rect);
		cout << msg << endl;

		res=g->GetBoard(msg);
		cout << msg << endl;

		getchar();
	}
}


/*

vertical wins
0 0 0 0 0 0
0 1 0 0 0 1
0 2 0 0 1 0
0 3 0 0 1 1

horizontal wins
0 0 0 0 0 0
1 0 0 0 0 1
2 0 0 0 1 0
3 0 0 0 1 1

diagonal wins
0 0 0 0 0 0
1 1 0 0 0 1
2 2 0 0 1 0
3 3 0 0 1 1

nothing
0 0 0 0 1 0
0 1 0 0 1 1
0 2 0 1 0 0
0 3 1 0 0 0


*/






