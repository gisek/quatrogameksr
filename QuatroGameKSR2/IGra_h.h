

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Tue Mar 05 23:26:39 2013
 */
/* Compiler settings for IGra.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __IGra_h_h__
#define __IGra_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IGra_FWD_DEFINED__
#define __IGra_FWD_DEFINED__
typedef interface IGra IGra;
#endif 	/* __IGra_FWD_DEFINED__ */


#ifndef __IGra_FWD_DEFINED__
#define __IGra_FWD_DEFINED__
typedef interface IGra IGra;
#endif 	/* __IGra_FWD_DEFINED__ */


#ifndef __Gra_FWD_DEFINED__
#define __Gra_FWD_DEFINED__

#ifdef __cplusplus
typedef class Gra Gra;
#else
typedef struct Gra Gra;
#endif /* __cplusplus */

#endif 	/* __Gra_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IGra_INTERFACE_DEFINED__
#define __IGra_INTERFACE_DEFINED__

/* interface IGra */
/* [uuid][object] */ 


EXTERN_C const IID IID_IGra;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A56D7A40-4FBF-480A-A6F0-2D7EF2164565")
    IGra : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE IsWon( 
            /* [out] */ boolean *isWon) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGraVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGra * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGra * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGra * This);
        
        HRESULT ( STDMETHODCALLTYPE *IsWon )( 
            IGra * This,
            /* [out] */ boolean *isWon);
        
        END_INTERFACE
    } IGraVtbl;

    interface IGra
    {
        CONST_VTBL struct IGraVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGra_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGra_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGra_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGra_IsWon(This,isWon)	\
    ( (This)->lpVtbl -> IsWon(This,isWon) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGra_INTERFACE_DEFINED__ */



#ifndef __KSRGra_LIBRARY_DEFINED__
#define __KSRGra_LIBRARY_DEFINED__

/* library KSRGra */
/* [version][helpstring][uuid] */ 



EXTERN_C const IID LIBID_KSRGra;

EXTERN_C const CLSID CLSID_Gra;

#ifdef __cplusplus

class DECLSPEC_UUID("E814DC57-2EF3-464B-AEFA-50783B535FEE")
Gra;
#endif
#endif /* __KSRGra_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


