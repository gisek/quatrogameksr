#include "../QuatroGameKSR2/IGra_h.h"
#include "../QuattroGameLibrary/GraFactory.h"
#include <iostream>

using namespace std;

// {A56D7A40-4FBF-480A-A6F0-2D7EF2164565}
//static const GUID CLSID_Gra =
//{ 0xa56d7a40, 0x4fbf, 0x480a, { 0xa6, 0xf0, 0x2d, 0x7e, 0xf2, 0x16, 0x45, 0x65 } };

int main(int argc, char* argv[])
{
 for (int i = 0; i < argc; i++)
 {
  if (strcmp(argv[i], "-Embedding") == 0  || strcmp(argv[i], "/Embedding" )== 0)
  {

   HRESULT hr;
   CoInitialize(NULL); 

   GraFactory *graFact = new GraFactory();
   graFact->AddRef();
   DWORD regID = 0;

   cout << "Registering class object" << endl;

   hr = CoRegisterClassObject(CLSID_Gra, (IClassFactory*)graFact, 
                CLSCTX_LOCAL_SERVER, REGCLS_SINGLEUSE, &regID);
   if ( FAILED(hr) )
   {
    cout << "Cannot register class object" << endl;
    CoUninitialize();
    exit(1);
   }

   cout << "Registered class object" << endl;
   Sleep(5000); // czas na utworzenie obiektu
   
   int a;
   cin >> a;
   /*do {
    Sleep(1000);
   } while( true );*/
   
   CoRevokeClassObject(regID); 
   graFact->Release();
   // Terminate COM.
   CoUninitialize(); 
  }
 }

 return 0;
}


//volatile ULONG usageCount;
//
//GraFactory::GraFactory(){
//	usageCount=0;
//}
//
//GraFactory::~GraFactory(){
//}
//
//HRESULT STDMETHODCALLTYPE GraFactory::QueryInterface(REFIID iid, void **ptr) {
//	if(ptr == NULL) return E_POINTER;
//	*ptr = NULL;
//	if(iid == IID_IUnknown) *ptr = this;
//	else if(iid == IID_IClassFactory) *ptr = this;
//	if(*ptr != NULL) { AddRef(); return S_OK; };
//	return E_NOINTERFACE;
//};
//
//ULONG STDMETHODCALLTYPE GraFactory::AddRef() {
//	InterlockedIncrement(&m_ref);
//	return m_ref;
//};
//
//ULONG STDMETHODCALLTYPE GraFactory::Release() {
//	ULONG rv = InterlockedDecrement(&m_ref);
//	if(rv == 0) delete this;
//	return rv;
//};
//
//HRESULT STDMETHODCALLTYPE GraFactory::LockServer(BOOL lock) {
//	if(lock) InterlockedIncrement(&usageCount);
//	else InterlockedDecrement(&usageCount);
//	return S_OK;
//};
//
//HRESULT STDMETHODCALLTYPE GraFactory::CreateInstance(IUnknown *outer,
//	REFIID iid, void **ptr) {
//	if(ptr == NULL) return E_POINTER;
//	*ptr = NULL;
//	if(iid != IID_IUnknown && iid != IID_IGra) return E_NOINTERFACE;
//	// chcemy dosta� NULL zamiast wyj�tku std::badalloc
//	Gra *obj = new (std::nothrow) Gra();
//	if(obj == NULL) return E_OUTOFMEMORY;
//	HRESULT rv = obj->QueryInterface(iid, ptr);
//	if(FAILED(rv)) { delete obj; *ptr = NULL; };
//	return rv;
//};